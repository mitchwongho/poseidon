package no.phishing;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;

import no.phishing.adapter.MenuAdapter;
import no.phishing.widget.utils.TextViewHelper;

/**
 *
 */
public class SplashActivity extends ActionBarActivity {

    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout mDrawer;
    private ListView mList;
    private MenuAdapter mMenuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_splash );

        //
        // Drawer stuff
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ViewGroup drawerContent = (ViewGroup) findViewById(R.id.drawer_content);
        mList = (ListView) findViewById(android.R.id.list);
        mMenuAdapter = new MenuAdapter( this );
        final TextView title = (TextView)findViewById( R.id.title );
        TextViewHelper.setTypeFace( this, title, TextViewHelper.TypefaceAsset.ROBOTO_LIGHT);
        mList.setAdapter( mMenuAdapter );
        mMenuAdapter.load(Arrays.asList( getResources().getStringArray( R.array.names ) ));
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawer.setDrawerListener(mActionBarDrawerToggle);
        //
        //
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        ab.setHomeButtonEnabled(true);
        ab.setDisplayUseLogoEnabled(false);

        final TextView appName = (TextView)findViewById( R.id.app_name );
        TextViewHelper.setTypeFace( this, appName, TextViewHelper.TypefaceAsset.ROBOTO_LIGHT );
        appName.setText(  getString(R.string.app_name).toUpperCase() );
        appName.setTextSize(40f);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mActionBarDrawerToggle.syncState();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mList.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent intent = new Intent( SplashActivity.this, MainActivity.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                intent.putExtra( MainActivity.EXTRA_STRING_PARTNER_NAME, mMenuAdapter.getItem( position ) );
                startActivity( intent );
                mDrawer.closeDrawers();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
                    return true;
                } else {
                    return super.onOptionsItemSelected(item);
                }
        }
    }
}
