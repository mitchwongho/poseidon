package no.phishing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import no.phishing.R;
import no.phishing.widget.utils.TextViewHelper;

/**
 *
 */
public class MenuAdapter extends ArrayAdapter<String> {

    private WeakReference<Context> mWeakContext;
    private List<String> mItems = new ArrayList<String>(10);

    static class ViewHolder {
        public TextView text1;
    }

    public MenuAdapter( Context context ) {
        super( context, android.R.layout.simple_list_item_1 );
        mWeakContext = new WeakReference<Context>( context );
    }

    public void load( List<String> items ) {
        mItems.clear();
        mItems.addAll( items );
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public String getItem(int position) {
        return mItems.get( position );
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = mWeakContext.get();
        if ( context == null ) {
            return super.getView(position, convertView, parent);
        } else {
            final String item = getItem( position );
            ViewHolder vh;
            if ( convertView == null ) {
                convertView = LayoutInflater.from( context ).inflate( R.layout.menu_list_item, parent, false );
                vh = new ViewHolder();
                vh.text1 = (TextView)convertView.findViewById( android.R.id.text1 );
                TextViewHelper.setTypeFace( context, vh.text1, TextViewHelper.TypefaceAsset.ROBOTO_LIGHT );
                convertView.setTag( vh );
            } else {
                vh = (ViewHolder)convertView.getTag();
            }
            //
            // Set Views
            vh.text1.setText( item );
            //
            return convertView;
        }

    }
}
