package no.phishing;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.UUID;

import no.phishing.widget.utils.TextViewHelper;

/**
 *
 */
public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_STRING_PARTNER_NAME = "EXTRA_STRING_PARTNER_NAME";

    private EditText digit0,digit1,digit2,digit3,digit4;
    private TextView mGenerateButton, mSendChallengeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView description = (TextView)findViewById( R.id.description );
        TextViewHelper.setTypeFace( this, description, TextViewHelper.TypefaceAsset.ROBOTO_THIN );
        digit0 = (EditText)findViewById(R.id.digit0);
        digit1 = (EditText)findViewById(R.id.digit1);
        digit2 = (EditText)findViewById(R.id.digit2);
        digit3 = (EditText)findViewById(R.id.digit3);
        digit4 = (EditText)findViewById(R.id.digit4);
        mGenerateButton = (TextView)findViewById( R.id.btn_generate );
        TextViewHelper.setTypeFace( this, mGenerateButton, TextViewHelper.TypefaceAsset.ROBOTO_LIGHT);
        mSendChallengeButton = (TextView)findViewById( R.id.btn_send_challenge );
        TextViewHelper.setTypeFace( this, mSendChallengeButton, TextViewHelper.TypefaceAsset.ROBOTO_LIGHT);
        //
        final String partnerName = getIntent().getStringExtra(EXTRA_STRING_PARTNER_NAME);
        description.setText( getString( R.string.here_is_a_random_code, partnerName ));

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle( partnerName );
        ab.setDisplayShowTitleEnabled(true);
        ab.setHomeButtonEnabled(true);
        ab.setDisplayUseLogoEnabled(false);
    }


    @Override
    protected void onResume() {
        super.onResume();
        generateCode();
        mGenerateButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateCode();
            }
        });

        mSendChallengeButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = MainActivity.this;
                final AlertDialog.Builder bldr = new AlertDialog.Builder( context );
                final TextView title = new TextView( context, null, R.style.Poseidon_TextAppearance_Large );
                title.setTextSize( 26 );
                title.setText( android.R.string.dialog_alert_title );
                title.setPadding( 8, 8, 8, 8);
                TextViewHelper.setTypeFace( context, title, TextViewHelper.TypefaceAsset.ROBOTO_MEDIUM );
                bldr.setCustomTitle( title );
                final TextView message = new TextView( context, null, R.style.Poseidon_TextAppearance_Large );
                message.setTextSize( 20 );
                message.setPadding( 8, 8, 8, 8);
                TextViewHelper.setTypeFace( context, message, TextViewHelper.TypefaceAsset.ROBOTO_THIN );
                message.setText( getString( R.string.this_code_will_expire, 5 ) );
                bldr.setView( message );
                bldr.setNegativeButton( android.R.string.cancel, null );
                bldr.setPositiveButton( R.string.send, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final Dialog dlg = bldr.create();
                dlg.setCanceledOnTouchOutside( false );
                dlg.show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return false;
            default:
                return false;
        }
    }

    private void generateCode() {
        final String uuid = UUID.randomUUID().toString();
        digit0.setText( new String(new char[] {uuid.charAt(0) }) );
        digit1.setText( new String(new char[] {uuid.charAt(1) }) );
        digit2.setText( new String(new char[] {uuid.charAt(2) }) );
        digit3.setText( new String(new char[] {uuid.charAt(3) }) );
        digit4.setText( new String(new char[] {uuid.charAt(4) }) );
    }
}
