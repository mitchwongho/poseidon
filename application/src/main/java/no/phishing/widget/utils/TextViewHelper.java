package no.phishing.widget.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class TextViewHelper {

    private static Map<TypefaceAsset, Typeface> mMap;

    public enum TypefaceAsset {
        ROBOTO_LIGHT("Roboto-Light.ttf"),
        ROBOTO_MEDIUM("Roboto-Medium.ttf"),
        ROBOTO_REGULAR("Roboto-Regular.ttf"),
        ROBOTO_THIN("Roboto-Thin.ttf");
        private String name;
        private TypefaceAsset( String name ) {
            this.name = name;
        }
        public String getName() {
            return this.name;
        }
    }

    static {
        mMap = new HashMap<TypefaceAsset, Typeface>(3);
    }

    public static void setTypeFace( final Context context, final TextView textView, final TypefaceAsset typefaceAsset ) {
        Typeface tf = mMap.get( typefaceAsset );

        if ( tf == null ) {
            tf = Typeface.createFromAsset( context.getAssets(), "fonts/"+typefaceAsset.getName() );
            mMap.put( typefaceAsset, tf );
        }
        textView.setTypeface( tf );

    }
}
