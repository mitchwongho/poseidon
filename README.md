Raise your hand if you've been contacted by a financial services provider in the last week?  
Chances are, you've had to verify yourself by providing a piece of personal information. But how did you verify the caller?

According to Consumer Reports, the cost of phishing is nearly $500 million per year in the United States alone [1] and 
the processes that are in-place to prevent phishing attacks, are there to look after the well-being of the corporation and 
not the citizens.

With Poseidon, citizens can take back control of the verification process using realtime challenge-response verification 
to ensure they are communicating with a trusted and verified organisation.  Poseidon is available to citizens as a 
smartphone app while organisations have access via the website.
